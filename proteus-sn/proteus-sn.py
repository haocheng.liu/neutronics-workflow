#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================
"""
Export script for Proteus-SN workflows
"""
import datetime
import logging
import os
import sys
sys.dont_write_bytecode = True

import smtk
import smtk.attribute
import smtk.io
import smtk.model

import rggsession

# Add the directory containing this file to the python module search list
import inspect
sys.path.insert(0, os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

from internal.writers import pyarc_writer

class Export(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "export Proteus-SN"

    def operateInternal(self):
        result = self.createResult(smtk.operation.Operation.Outcome.FAILED)

        try:
            success = ExportCMB(self.parameters(), self.log())
        except:
            smtk.ErrorMessage(self.log(), str(sys.exc_info()[0]))
            return result

        # Return with success
        if success:
            result = self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        return result

    def createSpecification(self):
        spec = self.createBaseSpecification()

        # Load export atts
        source_dir = os.path.abspath(os.path.dirname(__file__))
        sbt_path = os.path.join(source_dir, 'internal', 'proteus-sn-export.sbt')
        reader = smtk.io.AttributeReader()
        result = reader.read(spec, sbt_path, self.log())
        if result:
            print(self.log().convertToString(True))

        # opDef = spec.createDefinition('test op', 'operation')
        opDef = spec.findDefinition('proteus-sn-export')

        simulationDef = smtk.attribute.ResourceItemDefinition.New('attributes')
        simulationDef.setAcceptsEntries('smtk::attribute::Resource', '', True)
        opDef.addItemDefinition(simulationDef)

        modelDef = smtk.attribute.ResourceItemDefinition.New('model')
        modelDef.setAcceptsEntries('smtk::session::rgg::Resource', '', True)
        opDef.addItemDefinition(modelDef)

        resultDef = spec.createDefinition('result (' + self.name() + ')', 'result')

        return spec

ExportScope = type('ExportScope', (object,), dict())
# ---------------------------------------------------------------------
def ExportCMB(operator_spec, log):
    '''Entry function, called by CMB to write export files

    Returns boolean indicating success
    Parameters
    ----------
    spec: Top-level object passed in from CMB
    '''

    # Initialize scope instance to store spec values and other info
    scope = ExportScope()

    scope.logger = log
    scope.export_att = operator_spec
    scope.sim_atts = smtk.attribute.Resource.CastTo(operator_spec.find('attributes').value())
    if scope.sim_atts is None:
        msg = 'ERROR - No simlation attributes'
        print msg
        raise Exception(msg)

    model_entity = smtk.model.Entity.CastTo(operator_spec.find('model').objectValue(0))
    model_resource = model_entity.modelResource()
    if model_resource is None:
        msg = 'ERROR - No model'
        print msg
        raise Exception(msg)
    scope.model_entity = model_entity
    scope.model_manager = model_resource
    scope.model_path = model_resource.location()
    scope.model_file = os.path.basename(scope.model_path)

    # Initialize solver list
    solver_item = operator_spec.findString('Analysis')
    if solver_item is None:
        msg = 'Missing \"Analysis\" item -- cannot export'
        scope.logger.addError(msg)
        raise Exception(msg)
    if not solver_item.isSet(0):
        msg = 'Analysis item is not set -- cannot export'
        scope.logger.addError(msg)
        raise Exception(msg)
    solver_string = solver_item.value(0)
    #print 'solver_string:', solver_string
    scope.solver_list = solver_string.split('.')

    # Get output filename (path)
    output_path = None
    file_item = operator_spec.findFile('OutputFile')
    if file_item is not None:
        output_path = file_item.value(0)
    if not output_path:
        raise Exception('No output file specified')
    scope.output_path = output_path

    reload(proteus_sn_writer)
    writer = proteus_sn_writer.ProteusSNWriter()
    success = writer.write(scope)

    return success
