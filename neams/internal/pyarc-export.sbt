<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="pyarc-export" Label="Export to PyArc" BaseType="operation" Version="0">
      <ItemDefinitions>
        <Component Name="model" Label="Model">
          <Accepts>
            <Resource Name="smtk::session::rgg::Resource" Filter="model"/>
          </Accepts>
        </Component>
        <Resource Name="attributes" Label="Attributes">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <String Name="Analysis" Label="Analysis" Version="">
          <DiscreteInfo>
            <Value Enum="DIF3D">dif3d</Value>
            <Value Enum="MCC3">mcc3</Value>
            <Value Enum="MCC3 + DIF3D">mcc3.dif3d</Value>
          </DiscreteInfo>
        </String>
        <File Name="OutputFile" Label="Output File" Version="0" NumberOfRequiredValues="1"
          FileFilters="PyARC files (*.son);;All files (*.*)">
          <BriefDescription>The pyarc file to write</BriefDescription>
        </File>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
